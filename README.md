## This Project is not maintained anymore - Thanks to everyone who participated. (23.02.2017)


# Sprummlbot
The Sprummlbot is a fully customizable TeamSpeak3 Server Query bot.

## Features
 - AFK mover
 - Support notifier
 - Anti capture
 - Plugins
 - Web interface
 
## Version
0.5.2

## Downloads

### Executable .jar archive
Direct download: [Sprummlbot.jar](https://sprum.ml/download/latest)

`wget -O Sprummlbot.jar https://sprum.ml/download/latest`

## Tutorials
[Sprummlbot Documentations](https://sprum.ml/doc)

[Sprummlbot Installation](https://sprum.ml/forum/thread.php?id=1)

## License
[Apache License 2.0](LICENSE)
